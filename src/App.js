import React, { Component } from 'react';
import { connect } from 'react-redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Dashboard from './containers/Dashboard/Dashboard';
import Auth from './containers/Auth/Auth';
import UserConf from './containers/UserConf/UserConf';
import DefaultConf from './containers/DefaultConf/DefaultConf';
import ClientConf from './containers/ClientConf/ClientConf';

import * as actions from './store/actions/index';

import './App.css';

class App extends Component {
  componentDidMount () {
    this.props.onTryAutoSignup();
  }
  
  render() {
    let routes = (
      <Switch>
          <Route path="/asc/clientconf" component={ClientConf}/>
          <Route path="/asc/defaults" component={DefaultConf}/>
          <Route path="/asc/user" component={UserConf} />
          <Route path="/asc" component={Dashboard} />
          <Route path="/" exact component={Auth} />
      </Switch>
    )

    return (
      <BrowserRouter>
        {routes}
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch( actions.authCheckState() )
  };
};

export default connect( mapStateToProps, mapDispatchToProps )(App);
