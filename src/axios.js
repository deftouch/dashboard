import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:5501/'
});


export default instance;