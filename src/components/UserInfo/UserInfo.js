import React from 'react';
import classes from './UserInfo.module.css';

import {MAX_XP_POINTS} from '../../shared/constants';
import Aux from '../../hoc/Ax/Ax';
import { Form, Row, Col, Button, InputGroup } from 'react-bootstrap';

const UserInfo = (props) => {
    var xp_level;
    if(props.userInfo.xp_details != null){
        xp_level = parseInt(props.userInfo.xp_details.level)+1
    }
    return (
        <Aux>
            <Form onSubmit={props.onUserInfoSubmit} className={classes.UserInfo}>
                <Form.Group as = {Row}>
                    <Form.Label column sm={2} md={1} htmlFor="email">E-mail</Form.Label>
                    <Col sm={10} md={5}>
                        <Form.Control type="email" disabled defaultValue={props.userInfo.mail_id} id="mail_id" />
                    </Col>
                </Form.Group>
                <Row><h2>Currency</h2></Row>
                <Form.Group as = {Row}>
                    <Form.Label column sm={2} md={1} htmlFor="coins">Coins</Form.Label>
                    <Col sm={5} md={3}>
                        <Form.Control type="number" value= {props.userInfo.coins} required={true} pattern="\d+" id="coins" min="0" onChange={props.onUserInfoChange}/>
                    </Col>
                    <Form.Label column sm={2} md={1} htmlFor="gems">Gems</Form.Label>
                    <Col sm={5} md={3}>
                        <Form.Control type="number" value= {props.userInfo.gems} required={true} pattern="\d+" id="gems" min="0" onChange={props.onUserInfoChange}/>
                    </Col>
                </Form.Group>
                <Row><h2>Powerups</h2></Row>
                <Form.Group as = {Row}>
                    <Form.Label column sm={2} md={2} htmlFor="super_ball_count">Super Ball</Form.Label>
                    <Col sm={5} md={1}>
                        <Form.Control type="number" value= {props.userInfo.power_ups.super_ball_count} required={true} pattern="\d+" id="super_ball_count" min="0" max="1000" onChange={props.onUserInfoChange}/>
                    </Col>
                    <Form.Label column sm={2} md={{span:2, offset:1}} htmlFor="super_shot_count">Super Shot</Form.Label>
                    <Col sm={5} md={1}>
                        <Form.Control type="number" value= {props.userInfo.power_ups.super_shot_count} required={true} pattern="\d+" id="super_shot_count" min="0" max="1000" onChange={props.onUserInfoChange}/>
                    </Col>
                    <Form.Label column sm={2} md={{span: 3, offset:1}}htmlFor="bounce_indicator_count">Bounce Indicator Count</Form.Label>
                    <Col sm={5} md={1}>
                        <Form.Control type="number" value= {props.userInfo.power_ups.bounce_indicator_count} required={true} pattern="\d+" id="bounce_indicator_count" min="0" max="1000" onChange={props.onUserInfoChange}/>
                    </Col>
                </Form.Group>
                <Row><h2>XP</h2></Row>
                <Form.Group as = {Row}>
                        <Form.Label column sm={2} md={2} htmlFor="level">XP Level</Form.Label>
                        <Col sm={5} md={3}>
                            <Form.Control type="range" required={true} id="level" min="0" max="24" step="1" value={props.userInfo.xp_details.level} title={xp_level} onChange={props.onUserInfoChange}/>              
                        </Col>
                        <Form.Label column sm={2} md={{span:2, offset:1}} htmlFor="xp">XP Points</Form.Label>
                        <Col sm={5} md={4}>
                        <InputGroup>
                            <Form.Control type="number" value= {props.userInfo.xp_details.xp} required={true} pattern="\d+" id="xp" min="0" max={MAX_XP_POINTS[props.userInfo.xp_details.level]} onChange={props.onUserInfoChange}/>
                            <InputGroup.Append>
                                <InputGroup.Text>Max Points: {MAX_XP_POINTS[props.userInfo.xp_details.level]}</InputGroup.Text>
                            </InputGroup.Append>
                        </InputGroup>
                        </Col>
                </Form.Group>
                <Row><Col md={{span:1, offset:11}}><Button variant="primary" disabled={props.savingUserInfo} type="submit">Save</Button></Col></Row>
            </Form>
            <br/>
            <Form onSubmit={props.onTeamConfSubmit}>
                <Row>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text>Team Configuration</InputGroup.Text>
                        </InputGroup.Prepend>
                        {/* <TeamConf teamConf={props.userInfo.team_conf} onChange={props.onTeamConfChange}/> */}
                        <Form.Control as="textarea" rows="3" required={true} id="team_conf" value={props.userInfo.team_conf} onChange={props.onUserInfoChange}/><br/>
                    </InputGroup>
                </Row>
                <br/>
                <Row>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text>Unlocked Characters</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control as="textarea" rows="7" required={true} id="unlocked_chars" value={props.userInfo.unlocked_chars} onChange={props.onUserInfoChange}/>
                    </InputGroup>
                </Row>
                <br/>
                <Row><Col md={{span:1, offset:11}}><Button variant="primary" disabled={props.savingTeamConf} type="submit">Save</Button></Col></Row>
            </Form>
            <br/>
            <Form onSubmit={props.onPurchaseInfoSubmit}>
                <Row>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Purchased Characters</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control as="textarea" rows="2" id="character" value={props.userInfo.purchase_info.character} onChange={props.onUserInfoChange}/><br/>
                </InputGroup>
                </Row>
                <br/>
                <Row>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Purchased Shoes</InputGroup.Text>
                    </InputGroup.Prepend>
                <Form.Control as="textarea" rows="2" id="shoes" value={props.userInfo.purchase_info.shoes} onChange={props.onUserInfoChange}/><br/>
                </InputGroup>
                </Row>
                <br/>
                <Row>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Purchased Emoji</InputGroup.Text>
                    </InputGroup.Prepend>
                <Form.Control as="textarea" rows="2" id="emoji" value={props.userInfo.purchase_info.emoji} onChange={props.onUserInfoChange}/><br/>
                </InputGroup>
                </Row>
                <br/>
                <Row>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Purchased Emblems</InputGroup.Text>
                    </InputGroup.Prepend>
                <Form.Control as="textarea" rows="2" id="emblems" value={props.userInfo.purchase_info.emblems} onChange={props.onUserInfoChange}/><br/>
                </InputGroup>
                </Row>
                <br/>
                <Row>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Purchased Celebration</InputGroup.Text>
                    </InputGroup.Prepend>
                <Form.Control as="textarea" rows="2" id="celebration" value={props.userInfo.purchase_info.celebration} onChange={props.onUserInfoChange}/><br/>
                </InputGroup>
                </Row>
                <br/>
                <Row>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Purchased Jersey</InputGroup.Text>
                    </InputGroup.Prepend>
                <Form.Control as="textarea" rows="2" id="jersey" value={props.userInfo.purchase_info.jersey} onChange={props.onUserInfoChange}/><br/>
                </InputGroup>
                </Row>
                <br/>
                <Row>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Purchased Pack</InputGroup.Text>
                    </InputGroup.Prepend>
                <Form.Control as="textarea" rows="2" id="welcome_pack" value={props.userInfo.welcome_pack} onChange={props.onUserInfoChange}/><br/>
                </InputGroup>
                </Row>
                <br/>
                <Row><Col md={{span:1, offset:11}}><Button variant="primary" disabled={props.savingPurchaseInfo} type="submit">Save</Button></Col></Row>
            </Form>
        </Aux>
    );
}

export default UserInfo;