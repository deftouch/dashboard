import React from "react";


const removeByValue = (array, element) => {
    const index = array.indexOf(element);
    if (index > -1) {
      array.splice(index, 1);
    }
    return array
}

const TeamConf = (props) => {
    let bowlerId = props.teamConf.tmChar.map((x)=>x);
    let teamConf = props.teamConf.field.map((player,index) => {
        bowlerId = removeByValue(bowlerId, player.id)
        return (<tr key={index}>
            <td><input type="number" step="1" id="fielder_id" name={index} value={player.id} onChange={props.onChange}/></td>
            <td><input type="number" step="0.01" id="fielder_d" name={index} value={player.d} onChange={props.onChange}/></td>
            <td><input type="number" step="1" id="fielder_p" name={index} value={player.p} onChange={props.onChange}/></td>
            </tr>);
    });
    bowlerId = removeByValue(bowlerId, props.teamConf.wkpid);
    console.log(props);
    return (
        <div>
            <span>Wicket Keeper ID</span> &nbsp; <input id="wkpid" type="text" value={props.teamConf.wkpid} onChange={props.onChange}/>
            <p>Fielders</p>
            <table>
                <thead>
                <tr>
                    <th>Fielder ID</th>
                    <th>d</th>
                    <th>p</th>
                </tr>
                </thead>
                <tbody>
                {teamConf}
                </tbody>
            </table>
            <br/>
            <span>Bowler ID</span> &nbsp; <input id="bowler_id" type="number" step="1" value={bowlerId[0]} onChange={props.onChange}/>
        </div>
    );
}

export default TeamConf;