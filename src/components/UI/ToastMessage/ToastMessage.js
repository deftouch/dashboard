import React, {useState} from 'react';
import {Row, Col, Toast} from 'react-bootstrap';

const ToastMessage = (props) => {
    const [show, setShow] = useState(true);
    return (
      <Row>
        <Col xs={6}>
          <Toast onClose={() => setShow(false)} show={show} delay={props.delay} autohide>
            <Toast.Header>
              <img
                src="holder.js/20x20?text=%20"
                className="rounded mr-2"
                alt=""
              />
              <strong className="mr-auto">Deftouch Dashboard</strong>
              <small>11 mins ago</small>
            </Toast.Header>
            <Toast.Body>{props.message}</Toast.Body>
          </Toast>
        </Col>
      </Row>
    );
  }
  
 export default ToastMessage;