import React from  'react';
import classes from './Spinner.module.css';
const Spinner = (props) => {
    var containerClass = classes.container;
    var spinnerClass = classes.containerSpinner;

    if (props.variant === "buttonLoader") {
        containerClass = classes.buttonContainer;
        spinnerClass = classes.buttonSpinner;
    }
    return(
    <div className={containerClass}>
        <div className={spinnerClass}>
        <div></div><div><div></div></div>
        </div>
    </div>
    );
}

export default Spinner;