import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';
// import {withRouter} from 'react-router-dom';

import classes from './Auth.module.css';

import { updateObject } from '../../shared/utility';

import { Container, Row, Form, Button, InputGroup } from 'react-bootstrap';

class Auth extends Component {

    state = {
        username:"",
        password:""
    };

    handleSubmit(event) {
        event.preventDefault();
        this.props.onAuth(this.state.username,this.state.password);
    }

    inputChangedHandler = event => {
        this.setState(updateObject( this.state, {[event.target.id]: event.target.value}));
    }

    render(){

        let errorMessage = null;

        if ( this.props.error ) {
            errorMessage = (
                <p className={classes.error}>{this.props.error}</p>
            );
        }

        let authRedirect = null;
        if ( this.props.isAuthenticated ) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }

        return (
            <Container className={classes.Auth}>
                {authRedirect}
                <Form onSubmit={this.handleSubmit.bind(this)} className={classes.formWrapper}>
                    <Form.Group as = {Row}>
                        <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text><i className="fa fa-user-circle-o"></i></InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control id="username" type="text" placeholder="User Name" onChange={this.inputChangedHandler}/>
                        </InputGroup>
                    </Form.Group>
                    <Form.Group as = {Row}>
                        <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text><i className="fa fa-key"></i></InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control id="password" type="password" placeholder="Password" onChange={this.inputChangedHandler}/>
                        </InputGroup>
                    </Form.Group>
                    {errorMessage}
                    <center><Button disabled={this.props.isLoading} type="submit">Sign In</Button></center>
                </Form>
            </Container>
        );
    }
}


const mapStateToProps = state => {
    return {
        // loading: state.auth.loading,
        error: state.auth.error,
        isLoading: state.auth.loading,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(actions.auth(username, password))
    };
};

export default connect( mapStateToProps , mapDispatchToProps )( Auth );