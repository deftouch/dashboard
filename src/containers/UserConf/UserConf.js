import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import UserInfo from '../../components/UserInfo/UserInfo';
import * as actions from '../../store/actions/index';
import classes from './UserConf.module.css';
import { Container, Row, Form, Button, Col, InputGroup } from 'react-bootstrap';
import Spinner from '../../components/UI/Spinner/Spinner';

class UserConf extends Component {

    onUserIdChange = (event) => {
        this.id = event.target.value;
    }

    onSearchClicked = (event) => {
        event.preventDefault();
        this.props.onFetchUserInfo(this.id);
    }

    onUserConfSubmit = (event) => {
        event.preventDefault();
        this.props.onSaveUserInfo(this.props.userInfo)
    }

    onUserInfoChanged = (event) => {
        this.props.onUserInfoChanged(event.target.id, event.target.value);
    }


    // onUserTeamConfStructChanged = (event) => {
    //     // console.log(event.target.id, event.target.name, event.target.value)
    //     this.props.onUserTeamConfChanged(event.target.id, event.target.name, event.target.value);
    // }


    onTeamConfSubmit = (event) => {
        event.preventDefault();
        let data = {
            rid: this.props.userInfo.rid,
            team_conf: this.props.userInfo.team_conf,
            unlocked_chars: this.props.userInfo.unlocked_chars
        };
        console.log(data);
        this.props.onSaveUserTeamConf(data);
    }


    onPurchaseInfoSubmit = (event) => {
        event.preventDefault();
        let data = {
            rid: this.props.userInfo.rid,
            welcome_pack: this.props.userInfo.welcome_pack,
            purchase_info: this.props.userInfo.purchase_info
        };
        console.log(data);
        this.props.onSavePurchaseInfo(data);
    }

    render() {
        let userInfo = null;
        let authRedirect = null;
        let searchButtonContent = "Search";
        let searchBoxClass = classes.SearchBoxCenter;
        if (!this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }
        if (this.props.userInfo) {
            userInfo = (<UserInfo
                userInfo={this.props.userInfo} 
                onUserInfoChange={this.onUserInfoChanged} 
                onUserInfoSubmit={this.onUserConfSubmit}
                onTeamConfSubmit={this.onTeamConfSubmit} 
                onPurchaseInfoSubmit={this.onPurchaseInfoSubmit}
                savingUserInfo={this.props.isSavingUserInfo}
                savingTeamConf={this.props.isSavingTeamConf}
                savingPurchaseInfo={this.props.isSavingPurchasingInfo} />)
            searchBoxClass = classes.SearchBoxTop;
        }
        if (this.props.isSearching){
            searchButtonContent = <Spinner variant="buttonLoader"/>;
        }
        return (
            <Container>
                {authRedirect}
                <Row className={searchBoxClass}>
                    <Col xs={9} lg={{ span: 5, offset: 4 }}>
                        <Form onSubmit={this.onSearchClicked}>
                        <InputGroup>
                            <Form.Control type="text" placeholder="User ID" required={true} onChange={this.onUserIdChange} />
                            <InputGroup.Append>
                                <Button type="submit" className={classes.SearchButton} disabled={this.props.isSearching}> {searchButtonContent}</Button>
                            </InputGroup.Append>
                        </InputGroup>
                        </Form>
                    </Col>
                </Row>
                {userInfo}
            </Container>
        );
    }
}
const mapStateToProps = state => {
    return {
        userInfo: state.userConf.userInfo,
        isSearching: state.userConf.searching,
        isSavingUserInfo: state.userConf.savingUserInfo,
        isSavingTeamConf: state.userConf.savingTeamConf,
        isSavingPurchasingInfo: state.userConf.savingPurchaseInfo,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchUserInfo: (id) => dispatch(actions.fetchUserInfo(id)),
        onUserInfoChanged: (key, value) => dispatch(actions.userInfoChange(key, value)),
        onSaveUserInfo: (data) => dispatch(actions.saveUserInfo(data)),
        onSaveUserTeamConf: (data) => dispatch(actions.saveUserTeamConf(data)),
        onSavePurchaseInfo: (data) => dispatch(actions.savePurchaseInfo(data))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserConf);