import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';
import { Redirect } from 'react-router-dom';

import Spinner from '../../components/UI/Spinner/Spinner';

import classes from './ClientConf.module.css';
// import moment from 'moment';
import { Container, Row, Col, Form, InputGroup, Button } from 'react-bootstrap';


class ClientConf extends Component {

    componentDidMount() {
        if (this.props.isAuthenticated) {
            this.props.onFetchClientConf();
        }
    }

    onResetConf = (event) => {
        event.preventDefault();
        this.props.onResetConf(event.target.name);
    }

    saveClientConf = (event) => {
        event.preventDefault();
        this.props.onSaveClientConf(this.props.clientConf);
    }
    onClientConfChange = (event) => {
        // if(event.target.type === "checkbox"){
        //     this.props.onClientConfInputChange(event.target.id, event.target.checked);
        // } else if (event.target.type === "datetime-local"){
        //     this.props.onClientConfInputChange(event.target.id, moment(event.target.value).unix());
        // } else {
        this.props.onClientConfInputChange(event.target.id, event.target.value);
        // }
    }

    render() {
        let conf = null;
        let loading = null;
        let authRedirect = null;
        let saveButtonContent="Save Configuration";
        if (!this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }
        if (this.props.isLoading) {
            loading = (<Row><Col md={{offset:5}}><Spinner/></Col></Row>);
        }
        if (this.props.isSaving) {
            saveButtonContent = <Spinner variant="buttonLoader"/>;
        }
        if (this.props.clientConf && !this.props.isLoading) {
            conf = (
                <Form onSubmit={this.saveClientConf}>
                    <InputGroup>
                        <Form.Control as="textarea" rows="6" required={true} id="client_conf" value={this.props.clientConf.client_conf} onChange={this.onClientConfChange} />
                        <InputGroup.Append>
                            <Button name="client_conf" onClick={this.onResetConf}>Reset</Button>
                        </InputGroup.Append>
                    </InputGroup>
                    {/* <table>
                        <tbody>
                            <tr>
                                <td><label htmlFor="recovery_cost_gems">Recovery Gem Cost</label></td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="recovery_cost_gems" value={this.props.clientConf.recovery_cost_gems} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="recovery_time_in_mins">Recovery Time</label></td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="recovery_time_in_mins" value={this.props.clientConf.recovery_time_in_mins} onChange={this.onClientConfChange}/>&nbsp;<span>Mins</span></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="med_center_recovery_cost_cash">Recovery Gem Cost</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="med_center_recovery_cost_cash" value={this.props.clientConf.med_center_recovery_cost_cash} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="batting_guide_show_count">Batting guide show count</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="batting_guide_show_count" value={this.props.clientConf.batting_guide_show_count} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="is_bullet_time_enabled">Enable Bullet Time</label> &nbsp;</td>
                                <td><input type="checkbox" id="is_bullet_time_enabled" checked={this.props.clientConf.is_bullet_time_enabled} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="cash_per_objective">Cash Per Objective</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" id="cash_per_objective" required="true" pattern="\d+" value={this.props.clientConf.cash_per_objective} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="chest_reward">Chest Reward</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" id="chest_reward" required="true" pattern="\d+" value={this.props.clientConf.chest_reward} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="max_daily_cup_contribution">Max Daily Cup Contribution</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" id="max_daily_cup_contribution" required="true" pattern="\d+" value={this.props.clientConf.max_daily_cup_contribution} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="dpl_start_date">DPL Start Date</label> &nbsp;</td>
                                <td><input type="datetime-local" id="dpl_start_date" value={moment.unix(this.props.clientConf.dpl_start_date).format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS)} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="dpl_end_date">DPL End Date</label> &nbsp;</td>
                                <td><input type="datetime-local" id="dpl_end_date" value={moment.unix(this.props.clientConf.dpl_end_date).format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS)} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_perfect_ball">Energy Spent for Perfect ball</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_perfect_ball" value={this.props.clientConf.energy_spent_for_perfect_ball} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_red_ball_bowler">Energy Spent for Red ball bowler</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_red_ball_bowler" value={this.props.clientConf.energy_spent_for_red_ball_bowler} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_yellow_ball">Energy Spent for Yellow ball</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_yellow_ball" value={this.props.clientConf.energy_spent_for_yellow_ball} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_keeper">Energy Spent for Keeper</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_keeper" value={this.props.clientConf.energy_spent_for_keeper} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_spin_ball">Energy Spent for Spin Ball</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_spin_ball" value={this.props.clientConf.energy_spent_for_spin_ball} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_straight_ball">Energy Spent for Straight ball</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_straight_ball" value={this.props.clientConf.energy_spent_for_straight_ball} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_1_run">Energy Spent for 1 run</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_1_run" value={this.props.clientConf.energy_spent_for_1_run} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_2_run">Energy Spent for 2 run</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_2_run" value={this.props.clientConf.energy_spent_for_2_run} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_3_run">Energy Spent for 3 run</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_3_run" value={this.props.clientConf.energy_spent_for_3_run} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_dive">Energy Spent for dive</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_dive" value={this.props.clientConf.energy_spent_for_dive} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_non_dive">Energy Spent for non dive</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_non_dive" value={this.props.clientConf.energy_spent_for_non_dive} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="energy_spent_for_only_run">Energy Spent for only run</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="energy_spent_for_only_run" value={this.props.clientConf.energy_spent_for_only_run} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="penalty_runs_for_wide">Energy Spent for only run</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="penalty_runs_for_wide" value={this.props.clientConf.penalty_runs_for_wide} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="max_overs_normal_game">Max Overs for Normal Game</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" rclientConfequired="true" pattern="\d+" id="max_overs_normal_game" value={this.props.clientConf.max_overs_normal_game} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="max_overs_tutorial">Energy Spent for only run</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="max_overs_tutorial" value={this.props.clientConf.max_overs_tutorial} onChange={this.onClientConfChange}/></td>
                            </tr>
                            <tr>
                                <td><label htmlFor="max_overs_superover">Energy Spent for only run</label> &nbsp;</td>
                                <td><input type="number" step="1" min="0" required="true" pattern="\d+" id="max_overs_superover" value={this.props.clientConf.max_overs_superover} onChange={this.onClientConfChange}/></td>
                            </tr>
                        </tbody>
                    </table> */}
                    <br />
                    <Row>
                        <Col xs={12} md={{ offset: 5 }}>
                            <Button type="submit" variant="primary" className={classes.ClientConfSubmitBtn} disabled={this.props.isSaving}> {saveButtonContent}</Button>
                        </Col>
                    </Row>
                </Form>
            );
        }
        return (
            <Container>
                {authRedirect}
                <Row className={classes.Heading}><Col xs={12} md={{ offset: 4 }}><h1>Client Configuration</h1></Col></Row>
                <br />
                {loading}
                {conf}
                {/* <Toast message="Success" delay="3000" /> */}
            </Container>
        );
    }
};

const mapStateToProps = state => {
    return {
        clientConf: state.clientConf.conf,
        isSaving: state.clientConf.saving,
        isLoading: state.clientConf.loading,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchClientConf: () => dispatch(actions.fetchClientConf()),
        onClientConfInputChange: (key, value) => dispatch(actions.clientConfChange(key, value)),
        onSaveClientConf: (data) => dispatch(actions.saveClientConf(data)),
        onResetConf: (key) => dispatch(actions.resetClientConf(key))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClientConf);