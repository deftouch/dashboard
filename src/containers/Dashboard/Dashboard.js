import React, { Component } from 'react';
import classes from './Dashboard.module.css';

import { Container, Row, Col, ListGroup } from 'react-bootstrap'
import { connect } from 'react-redux';

import { Link, Redirect } from 'react-router-dom';

import * as actions from '../../store/actions/index';

class Dashboard extends Component {
    render() {
        let authRedirect = null;
        if (!this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }
        return (
            <Container fluid className={classes.Dashboard}>
                {authRedirect}
                <Row><Col md={{ span: 5, offset: 4 }}><h1 className={classes.Heading}> Choose Your Dashboard </h1></Col></Row>
                <Row>
                    <Col md={{ span: 2, offset: 5 }}>
                        <ListGroup horizontal className={classes.Menu}>
                            <ListGroup.Item><Link to="/asc/user" style={{ textDecoration: 'none' }}>User Configuration</Link></ListGroup.Item>
                            <ListGroup.Item><Link to="/asc/defaults" style={{ textDecoration: 'none' }}>Default Configuration</Link></ListGroup.Item>
                            <ListGroup.Item><Link to="/asc/clientconf" style={{ textDecoration: 'none' }}>Client Configuration</Link></ListGroup.Item>
                            <ListGroup.Item><Link to="/" onClick={this.props.onLogout} style={{ textDecoration: 'none' }}>Logout</Link></ListGroup.Item>
                        </ListGroup>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        authRedirectPath: state.auth.authRedirectPath,
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(actions.logout())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);