import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import classes from './DefaultConf.module.css';

import Aux from '../../hoc/Ax/Ax';

import Spinner from '../../components/UI/Spinner/Spinner';
import * as actions from '../../store/actions/index';
import { Container, Row, Col, Form, InputGroup, Button } from 'react-bootstrap';


class DefaultConf extends Component {

    componentDidMount() {
        if (this.props.isAuthenticated) {
            this.props.onFetchDefaultConf();
        }
    }

    saveDefaultConf = (event) => {
        event.preventDefault();
        this.props.onSaveDefaultConf(this.props.defaultConf);
    }

    onDefaultConfChange = (event) => {
        this.props.onDefaultConfInputChange(event.target.id, event.target.value);
    }

    onResetConf = (event) => {
        event.preventDefault();
        console.log(event.target.name)
        this.props.onResetConf(event.target.name)
    }

    render() {
        let conf = null;
        let loading = null;
        let authRedirect=null;
        let saveButtonContent="Save Configuration";
        if (!this.props.isAuthenticated) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }
        if (this.props.isLoading){
            loading = (<Row><Col md={{offset:5}}><Spinner/></Col></Row>);
        }
        if (this.props.isSaving) {
            saveButtonContent = <Spinner variant="buttonLoader"/>;
        }
        if (this.props.defaultConf && !this.props.isLoading) {
            conf = (
                <Aux>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Team Configuration</InputGroup.Text>
                            </InputGroup.Prepend>
                            {/* <TeamConf teamConf={props.userInfo.team_conf} onChange={props.onTeamConfChange}/> */}
                            <Form.Control as="textarea" rows="3" required={true} id="teamconfiguration" value={this.props.defaultConf.teamconfiguration} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="teamconfiguration" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Player Roster</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="playerroster" value={this.props.defaultConf.playerroster} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="playerroster" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Default Bot Conf</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="defaultbotconf" value={this.props.defaultConf.defaultbotconf} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="defaultbotconf" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Purchasing Items</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="purchasing_items" value={this.props.defaultConf.purchasing_items} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="purchasing_items" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Default Customization Bot</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="defaultcustomizationbot" value={this.props.defaultConf.defaultcustomizationbot} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="defaultcustomizationbot" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Default Unlock Characters</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="defaultunlockchar" value={this.props.defaultConf.defaultunlockchar} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="defaultunlockchar" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Team Configuration 1</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="teamconfiguration1" value={this.props.defaultConf.teamconfiguration1} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="teamconfiguration1" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Default Bot Conf 1</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="defaultbotconf1" value={this.props.defaultConf.defaultbotconf1} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="defaultbotconf1" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Bot Team Configuration Manager</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="botteamconfmanager" value={this.props.defaultConf.botteamconfmanager} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="botteamconfmanager" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Power Up Default Configuration</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="powerupdefaultconf" value={this.props.defaultConf.powerupdefaultconf} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="powerupdefaultconf" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Default Bot Conf 2</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="defaultbotconf2" value={this.props.defaultConf.defaultbotconf2} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="defaultbotconf2" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Team Configuration 2</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="teamconfiguration2" value={this.props.defaultConf.teamconfiguration2} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="teamconfiguration2" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>MatchMaking Level Offset</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control type="number" step="1" id="matchmaking_level_offset" min="0" max="100" required="true" pattern="\d+" value={this.props.defaultConf.matchmaking_level_offset} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="matchmaking_level_offset" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                    </Row>
                    <br/>
                    <Row>
                    <Col xs={12} md={{span:10, offset:1}}>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>Referral Rewards</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control as="textarea" rows="4" required={true} id="referral_rewards" value={this.props.defaultConf.referral_rewards} onChange={this.onDefaultConfChange} />
                            <InputGroup.Append>
                                <Button name="referral_rewards" onClick={this.onResetConf}>Reset</Button>
                            </InputGroup.Append>
                        </InputGroup> 
                    </Col>
                    </Row>
                    <br /><br />
                    <Row>
                    <Col xs={12} md={{offset:5}}>
                        <Button className={classes.DefaultConfSubmitBtn} disabled={this.props.isSaving} type="submit">
                            {saveButtonContent}
                        </Button>
                    </Col>
                    </Row>
                    <br/>
                </Aux>
            );
        }
        return (
            <Container>
                {authRedirect}
                <Row className={classes.Heading}><Col xs={12} md={{offset:4}}><h1>Default Configuration</h1></Col></Row>
                {loading}
                <Form onSubmit={this.saveDefaultConf}>
                    {conf}
                </Form>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        defaultConf: state.defaultConf.defaultConf,
        isLoading: state.defaultConf.loading,
        isAuthenticated: state.auth.token !== null,
        isSaving: state.defaultConf.saving,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchDefaultConf: () => dispatch(actions.fetchDefaultConf()),
        onSaveDefaultConf: (data) => dispatch(actions.saveDefaultConf(data)),
        onDefaultConfInputChange: (key, value) => dispatch(actions.defaultConfChange(key, value)),
        onResetConf: (key) => dispatch(actions.resetDefaultConf(key))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultConf);