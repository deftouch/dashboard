import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    conf: null,
    originalConf: null,
    saving: false,
    loading: false,
    error: null
}

const fetchClientConfStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const fetchClientConfSuccess = (state, action) => {
    return updateObject( state, { 
        conf: action.info,
        originalConf: action.info,
        saving: false,
        error: null,
        loading: false
     });
};

const fetchClientConfFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};

const saveClientConfSuccess = (state, action) => {
    return updateObject( state, {
        error: null,
        saving: false
     });
};

const saveClientConfFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        saving: false
    });
};

const saveClientConfStart = (state) => {
    return updateObject( state, { error: null, saving: true } );
};

const clientConfInputChange = (state, action) => {
    var updateInfo = {[action.key]: action.value};
    const updatedClientConf = updateObject(state.conf, updateInfo);
    const updatedState = {
        conf: updatedClientConf,
        error: null,
        loading: false
    }
	return updateObject(state, updatedState);
}

const resetInfo = (state, action) => {
    let updateInfo = null;
    updateInfo = {[action.key]: state.originalConf[action.key]};
    const updatedClientConf = updateObject(state.conf, updateInfo);
    const updatedState = {
        conf: updatedClientConf,
        error: null,
        loading: false
    }
	return updateObject(state, updatedState);    
}

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.FETCH_CLIENT_CONF_SUCCESS: return fetchClientConfSuccess(state, action);
        case actionTypes.FETCH_CLIENT_CONF_FAIL: return fetchClientConfFail(state, action);
        case actionTypes.FETCH_CLIENT_CONF_START: return fetchClientConfStart(state, action);
        case actionTypes.CLIENT_CONF_CHANGE: return clientConfInputChange(state, action);
        case actionTypes.SAVE_CLIENT_CONF_SUCCESS: return saveClientConfSuccess(state, action);
        case actionTypes.SAVE_CLIENT_CONF_FAIL: return saveClientConfFail(state, action);
        case actionTypes.SAVE_CLIENT_CONF_START: return saveClientConfStart(state, action);
        case actionTypes.RESET_CLIENT_CONF_CHANGE: return resetInfo(state, action);
        default: return state;
    }
}
export default reducer;