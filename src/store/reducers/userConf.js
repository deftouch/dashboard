import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';
import {MAX_XP_POINTS} from '../../shared/constants';

const initialState = {
	userInfo: null,
	searching: false,
	savingUserInfo:false,
	savingTeamConf: false,
	savingPurchaseInfo:false,
    loading: false,
    error: null
}

const userInfoStart = ( state, action ) => {
    return updateObject( state, { error: null, searching: true } );
};

const userInfoSuccess = (state, action) => {
    return updateObject( state, { 
        userInfo: action.info,
        error: null,
        searching: false
     });
};

const userInfoFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        searching: false
    });
};

const savePurchaseInfoSuccess = (state, action) => {
    return updateObject( state, {
        error: null,
        savingPurchaseInfo: false
     });
};

const savePurchaseInfoFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        savingPurchaseInfo: false
    });
};

const savePurchaseInfoStart = (state) => {
    return updateObject( state, { error: null, savingPurchaseInfo: true } );
};


const saveUserInfoSuccess = (state, action) => {
    return updateObject( state, {
        error: null,
        savingUserInfo: false
     });
};

const saveUserInfoFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        savingUserInfo: false
    });
};

const saveUserInfoStart = (state) => {
    return updateObject( state, { error: null, savingUserInfo: true } );
};

const saveTeamConfStart = (state) => {
    return updateObject( state, { error: null, savingTeamConf: true } );
};

const saveTeamConfSuccess = (state, action) => {
    return updateObject( state, {
        error: null,
        savingTeamConf: false
     });
};

const saveTeamConfFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        savingTeamConf: false
    });
};

const userInfoInputChange = (state, action) => {
	let updateInfo = null;
	switch(action.key) {
		case 'super_ball_count':
		case 'super_shot_count':
		case 'bounce_indicator_count':
			updateInfo = state.userInfo.power_ups;
			updateInfo[action.key] =  action.value;
			updateInfo = {"power_ups":updateInfo} 
			break;
		case 'level':
			updateInfo = state.userInfo.xp_details;
			updateInfo[action.key] =  action.value;
			updateInfo['xp'] = "0";
			updateInfo['targetXp'] = MAX_XP_POINTS[action.value]
			updateInfo = {"xp_details":updateInfo} 
			break;
		case 'xp':
			updateInfo = state.userInfo.xp_details;
			updateInfo[action.key] =  action.value;
			updateInfo = {"xp_details":updateInfo} 
			break;
		case 'character':
		case 'shoes':
		case 'emoji':
		case 'emblems':
		case 'celebration':
		case 'jersey':
			updateInfo = state.userInfo.purchase_info;
			updateInfo[action.key] =  action.value;
			updateInfo = {"purchase_info":updateInfo} 
			break;
		default:{
			updateInfo = {[action.key]: action.value}
			break;
		}
	}
	if(updateInfo != null) {
		const updatedUserInfo = updateObject(state.userInfo, updateInfo);
		const updatedState = {
			userInfo: updatedUserInfo,
			error: null,
			loading: false
		}
		return updateObject( state, updatedState );
	}
	return state
}

// const userConfChange = (state, action) => {
// 	let updateInfo = state.userInfo.team_conf;
// 	let newTmChar = null;
// 	switch(action.key) {
// 		case 'wkpid':
// 			updateInfo[action.key] = parseInt(action.value)
// 			newTmChar = updateInfo.field.map((player)=>{
// 				return player.id;
// 			});
// 			newTmChar[6] = updateInfo.wkpid
// 			newTmChar[7] = updateInfo.tmChar[7]
// 			updateInfo.tmChar = newTmChar
// 			break;
// 		case 'fielder_id':
// 			console.log("Fielder ID & index " + action.value + " " + action.index);
// 			updateInfo.field[action.index].id = parseInt(action.value)
// 			newTmChar = updateInfo.field.map((player)=>{
// 				return player.id;
// 			});
// 			newTmChar[6] = updateInfo.wkpid
// 			newTmChar[7] = updateInfo.tmChar[7]
// 			updateInfo.tmChar = newTmChar
// 			break;
// 		case "fielder_d":
// 			console.log("Fielder D & index " + action.value + " " + action.index);
// 			updateInfo.field[action.index].d = action.value;
// 			break;
// 		case "fielder_p":
// 			console.log("Fielder P & index " + action.value + " " + action.index);
// 			updateInfo.field[action.index].p = action.value;
// 			break;
// 		case "bowler_id":
// 			updateInfo.tmChar[7] = parseInt(action.value);
// 			break;
// 		default:
// 			break;
// 	}
// 	if(updateInfo != null) {
// 		updateInfo = {"team_conf":updateInfo}
// 		const updatedUserInfo = updateObject(state.userInfo, updateInfo);
// 		const updatedState = {
// 			userInfo: updatedUserInfo,
// 			error: null,
// 			loading: false
// 		}
// 		return updateObject( state, updatedState );
// 	}
// 	return state
// }

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.FETCH_PLAYER_SUCCESS: return userInfoSuccess(state, action);
        case actionTypes.FETCH_PLAYER_FAIL: return userInfoFail(state, action);
		case actionTypes.FETCH_PLAYER_START: return userInfoStart(state, action);
		case actionTypes.USER_INFO_CHANGE: return userInfoInputChange(state, action);
		case actionTypes.SAVE_PLAYER_INFO_SUCCESS: return saveUserInfoSuccess(state,action);
		case actionTypes.SAVE_PLAYER_INFO_FAIL: return saveUserInfoFail(state, action);
		case actionTypes.SAVE_PLAYER_INFO_START: return saveUserInfoStart(state);
		case actionTypes.SAVE_PLAYER_TEAM_CONF_SUCCESS: return saveTeamConfSuccess(state,action);
		case actionTypes.SAVE_PLAYER_TEAM_CONF_FAIL: return saveTeamConfFail(state, action);
		case actionTypes.SAVE_PLAYER_TEAM_CONF_START: return saveTeamConfStart(state);
		case actionTypes.SAVE_PURCHASE_INFO_SUCCESS: return savePurchaseInfoSuccess(state,action);
		case actionTypes.SAVE_PURCHASE_INFO_FAIL: return savePurchaseInfoFail(state, action);
		case actionTypes.SAVE_PURCHASE_INFO_START: return savePurchaseInfoStart(state);
		// case actionTypes.USER_TEAM_CONF_CHANGE: return userTeamConfChange(state, action);
        default: return state;
    }
};

export default reducer;