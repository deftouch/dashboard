import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    defaultConf: null,
    originalConf: null,
    saving: false,
    loading: false,
    error: null
}

const fetchDefaultConfStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const fetchDefaultConfSuccess = (state, action) => {
    return updateObject( state, { 
        defaultConf: action.info,
        originalConf: action.info,
        error: null,
        loading: false
     });
};

const fetchDefaultConfFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};

const saveDefaultConfSuccess = (state, action) => {
    return updateObject( state, {
        originalConf: action.newData,
        saving: false,
        error: null
     });
};

const saveDefaultConfFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        saving: false,
    });
};

const saveDefaultConfStart = (state) => {
    return updateObject( state, { error: null, saving: true } );
};

const defaultConfInputChange = (state, action) => {
    let updateInfo = null;
    if (action.key === "matchmaking_level_offset") {
        updateInfo = {[action.key]: parseInt(action.value)};
    } else {
        updateInfo = {[action.key]: action.value};
    }
    const updatedDefaultConf = updateObject(state.defaultConf, updateInfo);
    const updatedState = {
        defaultConf: updatedDefaultConf,
        error: null,
        loading: false
    }
	return updateObject(state, updatedState);
}

const resetInfo = (state, action) => {
    let updateInfo = null;
    if (action.key === "matchmaking_level_offset") {
        updateInfo = {[action.key]: state.originalConf[action.key]};
    } else {
        updateInfo = {[action.key]: state.originalConf[action.key]};
    }
    const updatedDefaultConf = updateObject(state.defaultConf, updateInfo);
    const updatedState = {
        defaultConf: updatedDefaultConf,
        error: null,
        loading: false
    }
	return updateObject(state, updatedState);    
}

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.FETCH_DEFAULT_CONF_SUCCESS: return fetchDefaultConfSuccess(state, action);
        case actionTypes.FETCH_DEFAULT_CONF_FAIL: return fetchDefaultConfFail(state, action);
        case actionTypes.FETCH_DEFAULT_CONF_START: return fetchDefaultConfStart(state, action);
        case actionTypes.SAVE_DEFAULT_CONF_SUCCESS: return saveDefaultConfSuccess(state, action);
        case actionTypes.SAVE_DEFAULT_CONF_FAIL: return saveDefaultConfFail(state, action);
        case actionTypes.SAVE_DEFAULT_CONF_START: return saveDefaultConfStart(state, action);
        case actionTypes.DEFAULT_CONF_CHANGE: return defaultConfInputChange(state, action);
        case actionTypes.RESET_DEFAULT_CONF_CHANGE: return resetInfo(state,action);
        default: return state;
    }
}
export default reducer;