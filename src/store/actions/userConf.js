import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchUserInfoStart = () => {
    return {
        type: actionTypes.FETCH_PLAYER_START
    };
};

export const fetchUserInfoFail = (error) => {
    return {
        type: actionTypes.FETCH_PLAYER_FAIL,
        error: error
    };
};

export const fetchUserInfoSuccess = (info) => {
    return {
        type: actionTypes.FETCH_PLAYER_SUCCESS,
        info: info
    };
};


export const userInfoChange = (key, value) => {
    return {
        type: actionTypes.USER_INFO_CHANGE,
        key: key,
        value: value
    };
}

export const userTeamConfChange = (key, value) => {
    return {
        type: actionTypes.USER_TEAM_CONF_CHANGE,
        key: key,
        value: value
    }
}

// export const userTeamConfStructChange = (key, index, value) => {
//     return {
//         type: actionTypes.USER_TEAM_CONF_CHANGE,
//         key: key,
//         value: value,
//         index: index
//     }
// }

export const fetchUserInfo = (id) => {
    return dispatch => {
        dispatch(fetchUserInfoStart());
        var accessToken = localStorage.getItem('token')
        const queryParams =  '?q='+id+'&is_email_id=false';
        axios.get('/player_info'+queryParams,{headers:{'Authorization':accessToken}}).then(
            res => {
                dispatch(fetchUserInfoSuccess(res.data))
            }
        ).catch( err => {
            dispatch(fetchUserInfoFail(err));
        });
    }
}

export const saveUserInfoStart = () => {
    return {
        type: actionTypes.SAVE_PLAYER_INFO_START
    };
};

export const saveUserInfoFail = (error) => {
    return {
        type: actionTypes.SAVE_PLAYER_INFO_FAIL,
        error: error
    };
};

export const saveUserInfoSuccess = () => {
    return {
        type: actionTypes.SAVE_PLAYER_INFO_SUCCESS
    };
};


export const saveUserInfo = (data) => {
    return dispatch => {
        dispatch(saveUserInfoStart());
        var accessToken = localStorage.getItem('token')
        axios.put('/player_info_update', data, {headers:{'Authorization':accessToken}}).then(
            res => {
                console.log("Save Success"+res.status);
                dispatch(saveUserInfoSuccess());
            }
        ).catch(
            err => {
                console.log(err);
                dispatch(saveUserInfoFail(err));
            }
        );
    }
}

export const saveUserTeamConfStart = () => {
    return {
        type: actionTypes.SAVE_PLAYER_TEAM_CONF_START
    };
};

export const saveUserTeamConfFail = (error) => {
    return {
        type: actionTypes.SAVE_PLAYER_TEAM_CONF_FAIL,
        error: error
    };
};

export const saveUserTeamConfSuccess = () => {
    return {
        type: actionTypes.SAVE_PLAYER_TEAM_CONF_SUCCESS
    };
};

export const savePurchaseInfoStart = () => {
    return {
        type: actionTypes.SAVE_PURCHASE_INFO_START
    };
};

export const savePurchaseInfoFail = (error) => {
    return {
        type: actionTypes.SAVE_PURCHASE_INFO_FAIL,
        error: error
    };
};

export const savePurchaseInfoSuccess = () => {
    return {
        type: actionTypes.SAVE_PURCHASE_INFO_SUCCESS
    };
};

export const saveUserTeamConf = (data) => {
    return dispatch => {
        dispatch(saveUserTeamConfStart());
        var accessToken = localStorage.getItem('token')
        axios.put('/update_team_conf', data, {headers:{'Authorization':accessToken}}).then(
            res => {
                console.log("Save Success"+res.status);
                dispatch(saveUserTeamConfSuccess());
            }
        ).catch(
            err => {
                console.log(err);
                dispatch(saveUserTeamConfFail(err));
            }
        );
    }
}

export const savePurchaseInfo = (data) => {
    return dispatch => {
        dispatch(savePurchaseInfoStart());
        var accessToken = localStorage.getItem('token')
        axios.put('/update_purchase_info', data,{headers:{'Authorization':accessToken}}).then(
            res => {
                console.log("Save Success"+res.status);
                dispatch(savePurchaseInfoSuccess());
            }
        ).catch(
            err => {
                console.log(err);
                dispatch(savePurchaseInfoFail(err));
            }
        );
    }
}