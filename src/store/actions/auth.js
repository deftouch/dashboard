import {
    AuthenticationDetails,
    CognitoUserPool,
    CognitoUser,
} from "amazon-cognito-identity-js";

import * as actionTypes from './actionTypes';

import {COGNITO_USER_POOL_ID, COGNITO_CLIENT_ID} from '../../shared/constants';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000);
    };
};

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    const POOL_DATA = {
        UserPoolId:COGNITO_USER_POOL_ID,
        ClientId:COGNITO_CLIENT_ID
      };
    var userPool = new CognitoUserPool(POOL_DATA);
    const cognitoUser = userPool.getCurrentUser();
    if (cognitoUser !== null) {
        cognitoUser.signOut()
        return {
            type: actionTypes.AUTH_LOGOUT
        };
    }
};

export const auth = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        var authenticationData = {
            Username: username,
            Password: password,
        };
        var authenticationDetails = new AuthenticationDetails(
            authenticationData  
        );
        const POOL_DATA = {
            UserPoolId:COGNITO_USER_POOL_ID,
            ClientId:COGNITO_CLIENT_ID
          };
        var userPool = new CognitoUserPool(POOL_DATA);
        var userData = {
            Username: username,
            Pool: userPool,
        };
        var cognitoUser = new CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess(response) {
              localStorage.setItem('token', response.getIdToken().getJwtToken());
              localStorage.setItem('expirationDate', new Date(response.getIdToken().getExpiration()*1000));
              localStorage.setItem('userId', response.getIdToken().payload['cognito:username']);
              dispatch(authSuccess(response.getIdToken().getJwtToken()));
            //   history.push("/home");  // or whatever route you want a signed in user to be redirected to
            },
            onFailure(err) {
              dispatch(authFail(err.message));
            },
          });
    };
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            logout();
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                logout();
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000 ));
            }   
        }
    };
};