import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchClientConfStart = () => {
    return {
        type: actionTypes.FETCH_CLIENT_CONF_START
    };
};

export const fetchClientConfFail = (error) => {
    return {
        type: actionTypes.FETCH_CLIENT_CONF_FAIL,
        error: error
    };
};

export const fetchClientConfSuccess = (info) => {
    return {
        type: actionTypes.FETCH_CLIENT_CONF_SUCCESS,
        info: info
    };
};

export const saveClientConfStart = () => {
    return {
        type: actionTypes.SAVE_CLIENT_CONF_START
    };
};

export const saveClientConfFail = (error) => {
    return {
        type: actionTypes.SAVE_CLIENT_CONF_FAIL,
        error: error
    };
};

export const saveClientConfSuccess = (newData) => {
    return {
        type: actionTypes.SAVE_CLIENT_CONF_SUCCESS,
        newData: newData
    };
};

export const clientConfChange = (key, value) => {
    return {
        type: actionTypes.CLIENT_CONF_CHANGE,
        key: key,
        value: value
    };
}

export const fetchClientConf = () => {
    return dispatch => {
        dispatch(fetchClientConfStart());
        var accessToken = localStorage.getItem('token')
        axios.get('/client_conf',{headers:{'Authorization':accessToken}}).then(
            res => {
                dispatch(fetchClientConfSuccess(res.data))
            }
        ).catch( err => {
            dispatch(fetchClientConfFail(err));
        });
    }
}

export const resetClientConf = (key) => {
    return {
        type: actionTypes.RESET_CLIENT_CONF_CHANGE,
        key: key
    };
}

export const saveClientConf = (data) => {
    return dispatch => {
        dispatch(saveClientConfStart());
        var accessToken = localStorage.getItem('token')
        axios.put('/update_client_conf', data, {headers:{'Authorization':accessToken}}).then(
            res => {
                console.log("Save Success"+res.status);
                dispatch(saveClientConfSuccess(data));
            }
        ).catch(
            err => {
                console.log(err);
                dispatch(saveClientConfFail(err));
            }
        );
    }
}