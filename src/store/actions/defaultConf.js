import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchDefaultConfStart = () => {
    return {
        type: actionTypes.FETCH_DEFAULT_CONF_START
    };
};

export const fetchDefaultConfFail = (error) => {
    return {
        type: actionTypes.FETCH_DEFAULT_CONF_FAIL,
        error: error
    };
};

export const fetchDefaultConfSuccess = (info) => {
    return {
        type: actionTypes.FETCH_DEFAULT_CONF_SUCCESS,
        info: info
    };
};

export const saveDefaultConfStart = () => {
    return {
        type: actionTypes.SAVE_DEFAULT_CONF_START
    };
};

export const saveDefaultConfFail = (error) => {
    return {
        type: actionTypes.SAVE_DEFAULT_CONF_FAIL,
        error: error
    };
};

export const saveDefaultConfSuccess = (newData) => {
    return {
        type: actionTypes.SAVE_DEFAULT_CONF_SUCCESS,
        newData: newData
    };
};

export const fetchDefaultConf = () => {
    return dispatch => {
        dispatch(fetchDefaultConfStart());
        var accessToken = localStorage.getItem('token')
        axios.get('/default_conf',{headers:{'Authorization':accessToken}}).then(
            res => {
                dispatch(fetchDefaultConfSuccess(res.data))
            }
        ).catch( err => {
            dispatch(fetchDefaultConfFail(err));
        });
    }
}

export const defaultConfChange = (key, value) => {
    return {
        type: actionTypes.DEFAULT_CONF_CHANGE,
        key: key,
        value: value
    };
}

export const resetDefaultConf = (key) => {
    return {
        type: actionTypes.RESET_DEFAULT_CONF_CHANGE,
        key: key
    };
}

export const saveDefaultConf = (data) => {
    return dispatch => {
        dispatch(saveDefaultConfStart());
        var accessToken = localStorage.getItem('token')
        axios.put('/update_default_conf', data, {headers:{'Authorization':accessToken}}).then(
            res => {
                console.log("Save Success"+res.status);
                dispatch(saveDefaultConfSuccess(data));
            }
        ).catch(
            err => {
                console.log(err);
                dispatch(saveDefaultConfFail(err));
            }
        );
    }
}